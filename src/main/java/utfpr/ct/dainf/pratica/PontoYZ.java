/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.pratica;

/**
 *
 * @author a1906453
 */
public class PontoYZ extends Ponto2D{
    private double x,y,z;
    
    public PontoYZ() {
        this.x = 0;
        this.y = 0;
        this.z = 0;
    }
    public PontoYZ(double y, double z) {
        this.x = 0;
        this.y = y;
        this.z = z;
    }

    @Override
    public String toString()
    {
        return getClass()+"("+getY()+getZ()+")";
    }
 
}
