package utfpr.ct.dainf.pratica;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 *
 * @author
 */
public class Ponto {
    private double x, y, z;

    public Ponto() {
        this.x = 0;
        this.y = 0;
        this.z = 0;
    }

    public Ponto(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getZ() {
        return z;
    }

    public void setZ(double z) {
        this.z = z;
    }
    @Override
    public String toString()
    {
        return  getClass()+"("+getX()+","+getY()+","+getZ()+")";
    }
    
    public boolean equals(Ponto a)
    {
        if(a==null)
            return false;
        else
            return x==a.x && y==a.y && z==a.z;
    }

    public double dist(Ponto a)
    {
        return Math.sqrt(Math.pow(a.x-x,2)+Math.pow(a.y-y,2)+Math.pow(a.z-z,2));
    }
    /**
     * Retorna o nome não qualificado da classe.
     * @return O nome não qualificado da classe.
     */
    public String getNome() {
        return getClass().getSimpleName();
    }

}
