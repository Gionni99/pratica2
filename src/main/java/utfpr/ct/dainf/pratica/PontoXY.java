/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.pratica;

/**
 *
 * @author a1906453
 */
public class PontoXY extends Ponto2D{
    private double x,y,z;

    public PontoXY() {
        this.x = 0;
        this.y = 0;
        this.z = 0;
    }
    public PontoXY(double x, double y) {
        this.x = x;
        this.y = y;
        this.z = 0;
    }

    @Override
    public String toString()
    {
        return getClass()+"("+getX()+getY()+")";
    }

    
}
