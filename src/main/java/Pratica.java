
import utfpr.ct.dainf.pratica.Ponto;
import utfpr.ct.dainf.pratica.Ponto2D;
import utfpr.ct.dainf.pratica.PontoXY;
import utfpr.ct.dainf.pratica.PontoXZ;
import utfpr.ct.dainf.pratica.PontoYZ;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * @author 
 */
public class Pratica {

    public static void main(String[] args) {
        PontoXZ a;
        a = new PontoXZ(-3.,2.);
        PontoXY b;
        b = new PontoXY(0.,2.);
        double distancia;
        distancia = a.dist(b);
        System.out.println(distancia);
    }
    
}
